package com.batch.job.learning.springbatchdemoexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBatchDemoExampleApplication {

	public static void main(String[] args) {
		System.exit(
				SpringApplication.exit(
						SpringApplication.run(SpringBatchDemoExampleApplication.class, args)
				)
			);
	}

}
