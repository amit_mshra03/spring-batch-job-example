package com.batch.job.learning.springbatchdemoexample.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@NoArgsConstructor
public class PersonDemo {
    private Integer person_id;
    private String first_name;
    private String last_name;

    public PersonDemo(String first_name, String last_name) {
        this.first_name = first_name;
        this.last_name = last_name;
    }

}

